#!/usr/bin/python3
from node import Node
from pairing_heap_interface import PairingHeapInterface
from pairing_heap_standard_v2 import PairingHeapStandard
from pairing_heap_multipass import PairingHeapMultipass
from pairing_heap import PairingHeap

if __name__=="__main__":
	MODES={0:"Standard", 1:"FrontToBack", 2:"BackToFront", 3:"Multipass", 4:"Lazy"}
	testkeys=[5,1,3,7,4,11,18,2,0]
	heap=PairingHeap(4)
	heap.make_heap()
	node7=None
	node3=None
	for key in testkeys:
		node=Node(key)
		if key==7:
			node7=node
		elif key==3:
			node3=node
		heap.insert(node)
	heap.delete_min()
	heap.delete_min()
	heap.delete_min()

	node91=None
	node17=None
	node8=None
	testkeys2=[9, 12, 8, 91, 17, 6, 13]
	heap2=PairingHeap(4)
	heap2.make_heap()
	for key in testkeys2:
		node=Node(key)
		if key==91:
			node91=node
		elif key==17:
			node17=node
		elif key==8:
			node8=node
		heap2.insert(node)
	heap.merge(heap2.heap)
	heap.delete(node7)
	heap.delete(node91)
	heap.delete(node8)
	heap.decrease_key(node17, 7)
	heap.decrease_key(node3, 2)
