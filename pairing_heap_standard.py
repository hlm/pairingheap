#!/usr/bin/python3
from node import Node
from pairing_heap_interface import PairingHeapInterface

class PairingHeapStandard(PairingHeapInterface):
	#TODO: find out if key multiplicity is permitted (if so need to introduce some node ID?)
	#TODO count link operations
	#TODO: left/right child (page 115)
	def __init__(self, root=None):
		self.root=root
		if self.root==None:
			print("created empty heap")
		#else:
		#	print("created heap with root {}".format(self.root.key))

	def make_heap(self):
		#this is equivalent to init
		pass
	
	def listInorder(self, root):
		if(root==None):
			return []
		return self.listInorder(root.leftChild)+[root.key]+self.listInorder(root.nextSibling)

	def find_min(self):
		if self.root==None:
			return None
		else:
			return self.root

	def insert(self, node):
		print("trying to insert {}...".format(node.key))
		if self.root==None: 
			#heap was empty before
			self.root=node
		else:
			newheap=PairingHeapStandard(node)
			self.merge(newheap)
		#elif self.root.leftChild==None:
			#heap consisted only of root node before
		#	self.root.leftChild=node
		#else:
			#current left child becomes next sibling of inserted node
		#	node.nextSibling=self.root.leftChild
		#	self.root.leftChild=node
		print(self.listInorder(self.root))
		pass

	def delete_min(self):
		print("trying to delete min...")
		if self.root==None:
			print("Heap was already empty.")
			return
		elif self.root.leftChild==None:
			#heap contained only one element
			self.root=None
		elif self.root.leftChild.nextSibling==None:
			#first child has no siblings->first child becomes root
			self.root=self.root.leftChild
		else:
			self.root=self.root.leftChild
			current=self.root
			nextSibling=None
			heaps=[]
			paired=[]
			#roots=[]
			#TODO use merge function for this
			#left-to-right pairing pass v2
			while current!=None:#create heaps of all orphaned children
				nextSibling=current.nextSibling
				heaps+=[PairingHeapStandard(current)]
				current.nextSibling=None
				current=nextSibling
			for j in range(0,len(heaps),2):
				if(j==len(heaps)-1):#last one
					paired+=[heaps[j]]
				else:
					heap=heaps[j]
					heap.merge(heaps[j+1])
					paired+=[heap]
			#combining backwards pass v2
			combined=paired[-1]
			for i in range(len(paired)-2, -1, -1):
				combined.merge(paired[i])
			self.root=combined.root

			"""#left-to-right pairing pass v1
			while current!=None:
				if current.nextSibling==None:
					roots+=[current]
					break
				else:
					nextS=current.nextSibling.nextSibling
					if current.key<=current.nextSibling.key:
						current.nextSibling.nextSibling=current.leftChild
						current.leftChild=current.nextSibling
						current.nextSibling=None
						if current.leftChild.nextSibling==None:
							current.leftChild.parent=current
						roots+=[current]
					else:
						newRoot=current.nextSibling
						current.nextSibling=newRoot.leftChild
						newRoot.leftChild=current
						if current.nextSibling==None:
							current.parent=newRoot
						roots+=[newRoot]
					current=nextS"""
			"""#combining backwards pass v1
			currentRoot=roots[-1]
			for i in range(len(roots)-2, -1, -1):
				nextRoot=roots[i]
				if currentRoot.key<=nextRoot.key:
					nextRoot.nextSibling=currentRoot.leftChild
					currentRoot.leftChild=nextRoot
					if nextRoot.nextSibling==None:
						nextRoot.parent=currentRoot
				else:
					currentRoot.nextSibling=nextRoot.leftChild
					nextRoot.leftChild=currentRoot
					if currentRoot.nextSibling==None:
						currentRoot.parent=nextRoot
					currentRoot=nextRoot
			self.root=currentRoot"""
		print("result is {}".format(self.listInorder(self.root)))	
		pass

	def merge(self, heap2):
		print("Trying to merge {} and {}...".format(self.listInorder(self.root), self.listInorder(heap2.root)))
		if self.root==None:#heap is empty
			self.root=heap2.root
		elif heap2.root==None:#heap 2 is empty
			pass #this heap is the result
		else:
			if self.root.key<=heap2.root.key:
				heap2.root.nextSibling=self.root.leftChild
				if heap2.root.nextSibling==None:
					heap2.root.parent=self.root
				#if self.root.leftChild!=None:
				#	self.root.leftChild.leftOnly=False#just in case
				#elif heap2.root.leftChild==None:#heap2 is a single node
				#	heap2.root.leftOnly=True#will become a left only child
				#	heap2.root.parent=self.root
				self.root.leftChild=heap2.root
			else:
				self.root.nextSibling=heap2.root.leftChild
				if self.root.nextSibling==None:
					self.root.parent=heap2.root
				#if self.root.nextSibling!=None:
				#	self.root.nextSibling.leftOnly=False
				#elif self.root.leftChild==None:#heap is a single node
				#	self.root.leftOnly=True#will become a left only child
				#	self.root.parent=heap2.root
				heap2.root.leftChild=self.root
				self.root=heap2.root
			#TODO check for only children?
		print("Result is {}".format(self.listInorder(self.root)))

	def decrease_key(self, node, diff):#TODO more testing
		if self.root.key==node.key:
			self.root.key=self.root.key-diff
		else:
			#first step: cut node from heap
			self.unlink_node(node)#helper function
			#second step: decrease key
			subheap=PairingHeapStandard(node)
			subheap.root.key=subheap.root.key-diff
			#third step: merge back in
			self.merge(subheap)

	def delete(self, node): #TODO more testing?
		print("trying to delete {} from {}".format(node.key, self.listInorder(self.root)))
		if self.root.key==node.key:
			self.delete_min()
		else:
			self.unlink_node(node)#helper function
					
			subheap=PairingHeapStandard(node)
			subheap.delete_min()
			self.merge(subheap)
		print("result is {}".format(self.listInorder(self.root)))
		pass

	def unlink_node(self, node):
		#removes node from heap updating pointers
		if self.root.key==node.key:#remove the whole heap
			self.root=None
		else:
			if node.nextSibling!=None:
				temp=node.nextSibling
				while temp.nextSibling!=None:
					temp=temp.nextSibling
				if temp.parent.leftChild.key==node.key:#node is leftmost child (this comp requires unique keys!)
					#link parent to next sibling
					temp.parent.leftChild=node.nextSibling
					node.nextSibling=None
				else:
					#node is neither first nor last child of parent
					prevSibling=temp.parent.leftChild
					while prevSibling.nextSibling.key!=node.key:#find left (previous) sibling
						prevSibling=prevSibling.nextSibling
					prevSibling.nextSibling=node.nextSibling #cut out node, link left and right sibling
			else:
				#node is rightmost child of parent
				if node.parent.leftChild.key==node.key:
					#node is only child: just remove
					node.parent.leftChild=None
				else:
					prevSibling=node.parent.leftChild
					while prevSibling.nextSibling.key!=node.key:#find left (previous) sibling
						prevSibling=prevSibling.nextSibling
					prevSibling.parent=node.parent
					prevSibling.nextSibling=None
			node.parent=None	
		
			
