#!/usr/bin/python3
from random import shuffle
import sys
from node import Node
from pairing_heap_interface import PairingHeapInterface
from pairing_heap_standard_v2 import PairingHeapStandard
from pairing_heap_multipass import PairingHeapMultipass
from pairing_heap import PairingHeap

NUMBER_TESTS=100
LIST_LEN=10000#number of elements in test list
TYPES={0:"Standard", 1:"FrontToBack", 2:"BackToFront", 3:"Multipass", 4:"Lazy"}

def isSorted(list0):
	return all(list0[i] < list0[i + 1] for i in range(len(list0)-1))


if __name__=="__main__":
	testInput=[x for x in range(LIST_LEN)]
	testOutputCount=[]
	minCounts=[sys.maxsize for k in range(len(TYPES))]
	maxCounts=[0 for l in range(len(TYPES))]
	for _ in range(NUMBER_TESTS):
		shuffle(testInput) #pseudo-random permutation in-place (running all types on the same permutation)
		for heapType in range(5):
			count=0
			testOutput=[]
			heap=PairingHeap(heapType)
			heap.make_heap()
			for element in testInput:
				node=Node(element)
				count+=heap.insert(node)
			for i in range(len(testInput)):
				(minKey, c)=heap.delete_min()
				testOutput+=[minKey]
				count+=c
			if isSorted(testOutput):
				maxCounts[heapType]=max(maxCounts[heapType], count)
				minCounts[heapType]=min(minCounts[heapType], count)
			print("pairing heap of type {} needed {} linking operations; result is correct: {}".format(TYPES[heapType], count, isSorted(testOutput)))
	for heapType in range(5):
		print("pairing heap of type {} needed at least {} and at most {} linking operations".format(TYPES[heapType], minCounts[heapType], maxCounts[heapType]))
	
