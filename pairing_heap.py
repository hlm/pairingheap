#!/usr/bin/python3
from node import Node
from pairing_heap_interface import PairingHeapInterface
from pairing_heap_standard_v2 import PairingHeapStandard
from pairing_heap_multipass import PairingHeapMultipass
from pairing_heap_ftb import PairingHeapFTB
from pairing_heap_btf import PairingHeapBTF
from pairing_heap_lazy import PairingHeapLazy

class PairingHeap(PairingHeapInterface):
	MODES={0:"Standard", 1:"FrontToBack", 2:"BackToFront", 3:"Multipass", 4:"Lazy"}
	mode=0
	heap=None
	def __init__(self, mode=0):
		self.mode=mode

	def make_heap(self):
		if self.mode==0:
			self.heap=PairingHeapStandard()
		elif self.mode==1:
			self.heap=PairingHeapFTB()
		elif self.mode==2:
			self.heap=PairingHeapBTF()
		elif self.mode==3:
			self.heap=PairingHeapMultipass()
		elif self.mode==4:
			self.heap=PairingHeapLazy()
		else:
			print("Invalid heap ID! No heap of type ID {} is implemented. Defaulting to standard heap.".format(self.mode))
			self.heap=PairingHeapStandard()
	
	def find_min(self):
		#Careful! Implementation is inconsistent across pairing heap types. TODO.
		return self.heap.find_min()

	def insert(self, node):
		#inserts node; returns number of linking operations performed
		return self.heap.insert(node)

	def delete_min(self):
		#deletes min; returns number of linking operations performed
		return self.heap.delete_min()
		
	def merge(self, heap2):
		#merges this heap and heap 2; returns number of linking operations performed
		return self.heap.merge(heap2)

	def delete(self, node):
		self.heap.delete(node)

	def decrease_key(self, node, diff):
		self.heap.decrease_key(node, diff)
