#!/usr/bin/python3
from node import Node
from pairing_heap_interface import PairingHeapInterface

class PairingHeapBTF(PairingHeapInterface):
	#TODO: find out if key multiplicity is permitted (if so need to introduce some node ID?)
	#TODO count link operations
	#TODO: left/right child (page 115)
	def __init__(self, root=None):
		self.root=root
		if self.root==None:
			print("created empty heap")
		else:
			self.root.nextSibling=self.root

	def make_heap(self):
		#this is equivalent to init
		pass
	
	def listInorder(self, root, startSibling=None):
		if(root==None):
			return []
		elif startSibling==None:
			return self.listInorder(root.leftChild, None)+[root.key]+self.listInorder(root.nextSibling, root)
		elif root.key==startSibling.key:
			return []
		return self.listInorder(root.leftChild, None)+[root.key]+self.listInorder(root.nextSibling, startSibling)
		#return "TODO"

	def find_min(self):
		if self.root==None:
			return None
		else:
			return self.root

	def insert(self, node):
		#links node to root; returns number of linking operations
		linkCount=0
		#print("trying to insert {}...".format(node.key))
		if self.root==None: 
			#heap was empty before
			self.root=node
			self.root.nextSibling=self.root
		else:
			newheap=PairingHeapBTF(node)
			linkCount+=self.merge(newheap)
		#print(self.listInorder(self.root))
		return linkCount

	def delete_min(self):
		#deletes current root and restructures heap; returns number of linking operations
		minKey=None
		linkCount=0
		#print("trying to delete min...")
		if self.root==None:
			print("Heap was already empty.")
			return (minKey, linkCount)
		elif self.root.leftChild==None:
			#heap contained only one element
			minKey=self.root.key
			self.root=None
		elif self.root.leftChild.nextSibling.key==self.root.leftChild.key:#first child points to itself
			#first child has no siblings->first child becomes root
			minKey=self.root.key
			self.root=self.root.leftChild
		else:
			minKey=self.root.key
			self.root=self.root.leftChild
			current=self.root.nextSibling
			nextSibling=None
			heaps=[]
			combined=None
			#left-to-right pairing+combining pass
			while current.key!=self.root.key:#create heaps of all orphaned children
				nextSibling=current.nextSibling
				heaps+=[PairingHeapBTF(current)]
				current.nextSibling=current
				current=nextSibling
			heaps+=[PairingHeapBTF(self.root)]
			self.root.nextSibling=self.root
			for j in range(0,len(heaps),2):
				if(j==len(heaps)-1):#last one
					if combined==None:
						combined=heaps[j]#if there is only one child
					else:
						linkCount+=combined.merge(heaps[j])#combine with the rest
				else:
					heap=heaps[j]
					linkCount+=heap.merge(heaps[j+1])#pairing
					if combined==None:
						combined=heap#first pair
					else:
						linkCount+=combined.merge(heap)#combining
			self.root=combined.root
		#print("result is {}".format(self.listInorder(self.root)))	
		return (minKey, linkCount)

	def merge(self, heap2):
		#links two heaps together; returns number of linking operations
		linkCount=0
		#print("Trying to merge {} and {}...".format(self.listInorder(self.root), self.listInorder(heap2.root)))
		if self.root==None:#heap is empty
			self.root=heap2.root
		elif heap2.root==None:#heap 2 is empty
			pass #this heap is the result
		else:
			if self.root.key<=heap2.root.key:
				if self.root.leftChild!=None:
					#link heap2.root into list of children
					heap2.root.nextSibling=self.root.leftChild.nextSibling
					self.root.leftChild.nextSibling=heap2.root
				heap2.root.parent=self.root#TODO think about better pointer solution
				self.root.leftChild=heap2.root
				linkCount=1
			else:
				if heap2.root.leftChild!=None:
					#link root into list of children
					self.root.nextSibling=heap2.root.leftChild.nextSibling
					heap2.root.leftChild.nextSibling=self.root
				self.root.parent=heap2.root
				heap2.root.leftChild=self.root
				self.root=heap2.root
				linkCount=1
		#print("Result is {}".format(self.listInorder(self.root)))
		return linkCount

	def decrease_key(self, node, diff):#TODO more testing
		if self.root.key==node.key:
			self.root.key=self.root.key-diff
		else:
			#first step: cut node from heap
			self.unlink_node(node)#helper function
			#second step: decrease key
			subheap=PairingHeapBTF(node)
			subheap.root.key=subheap.root.key-diff
			#third step: merge back in
			self.merge(subheap)

	def delete(self, node): #TODO more testing?
		print("trying to delete {} from {}".format(node.key, self.listInorder(self.root)))
		if self.root.key==node.key:
			self.delete_min()
		else:
			self.unlink_node(node)#helper function
					
			subheap=PairingHeapBTF(node)
			subheap.delete_min()
			self.merge(subheap)
		print("result is {}".format(self.listInorder(self.root)))
		pass

	def unlink_node(self, node):
		#removes node from heap updating pointers
		if self.root.key==node.key:#remove the whole heap
			self.root=None
		else:
			if node.nextSibling.key!=node.key:#if node is not an only child
				predecessor=node.nextSibling
				while node.key!=predecessor.nextSibling.key:
					predecessor=predecessor.nextSibling
				predecessor.nextSibling=node.nextSibling
				if node.parent.leftChild.key==node.key:
					node.parent.leftChild=predecessor
				node.nextSibling=node	
			else:
				node.parent.leftChild=None
			node.parent=None	
		
			
