# git commands

### Setting up and navigating local branches
**In general, avoid working directly on the master branch**. Before you start coding, navigate to the project in your terminal, then use
`git status` to check in what branch you currently are. You can find the name of your current branch in the first line of the output.
If you are on branch master:
    - use `git checkout -b name-of-your-branch` to create a new branch and switch to it.
    - alternatively, if you want to switch to an already existing branch, use `git checkout name-of-existing-branch`
    
Once you are in your branch, you can do whatever you want. At any time, you can use `git status` to see a list of files that you have changed or created in your branch.
If you forget the names of your local branches, you can type `git branch` to see a list of all local branches.

### If you have been working locally in master
Create a new local branch containing all your local changes (committed and uncommitted) by calling `git branch name-of-new-target-branch`.
Then (and only then) reset the master branch to the repository version with `git reset --hard`. This will delete all your local changes in this branch, so make sure you have created the new local branch first.
Finally, do `git checkout name-of-new-target-branch` to continue working in the new branch.

### Uploading code
To upload your changes to the git server, do the following:
    - Add your changes to the staging area. This basically means that you are satisfied with these changes and earmark them to become part of the version you are going to upload.
    If you want to use all of the changes you made, simply type `git add -A` to add everything at once.
    Otherwise, add each file separately using `git add path-to-file` where path-to-file is the path to the file that shows up in the git status output.
    - Commit your changes. When you use commit, everything that was in the staging area becomes a part of the definite version of your code that you want to upload.
    Do this with `git commit -m "commit-message"`, where commit-message is a short message describing what your new code does, e.g. "finished publisher for distance sensor"
    Note that you also need to add and commit (but not necessarily push) any changes you want to keep before you can switch fo a different local branch.
    - Push your new version to the server.
    If you have never pushed from your current branch before, type `git push -u origin name-of-your-branch` to tell the server about your new branch.
    For all subsequent pushes from this branch, you can simply use `git push`.

Once you have pushed your code, you should be able to see it on the git platform in your browser under Repository/Branches. 
If you are finished with your work on this branch and want to add it to the finished code in the master branch, go to Merge Requests in the browser 
and set up a new merge request from your branch to the master branch.


### Downloading changes made by others
**To get the newest version of the master branch,**
navigate to the master branch in your terminal (using `git checkout master`) and run `git pull`.

**To get a different branch from the server,**
use `git checkout --track origin/name-of-branch`. This will create an equivalent local branch in your project. You can use `git pull` and `git push` to pull or publish changes like on any other branch.

