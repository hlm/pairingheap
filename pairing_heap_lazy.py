#!/usr/bin/python3
from node import Node
from pairing_heap_interface import PairingHeapInterface

class PairingHeapLazy(PairingHeapInterface):
	forest=[] #list storing roots of all top-level trees
	def __init__(self, root=None):
		self.forest=[]
		if root!=None:
			root.parent=None
			self.forest+=[root]

	def listInorder(self):
		forestList=[]
		for root in self.forest:
			forestList+=[self.listInorderTree(root)]
		return forestList

	def listInorderTree(self, root):
		if root==None:
			return []
		else:
			return self.listInorderTree(root.leftChild)+[root.key]+self.listInorderTree(root.nextSibling)

	def insert(self, node):
		#concatenates node to list of trees; returns number of linking ops (always 0) for sake of consistency
		#print("trying to insert {}...".format(node.key))
		if node==None:
			return 0
		node.parent=None
		self.forest+=[node]
		#print("Result of insert is {}.".format(self.listInorder()))
		return 0	

	def find_min(self):
		#performs pairing pass and returns min + number of linking operations
		linkCount=0
		if len(self.forest)==0: #heap is empty
			return (None, linkCount)
		elif len(self.forest)==1:
			return (self.forest[0], linkCount)
		else:
			currentMin=self.forest[0]
			pairedForest=[]
			index=-1
			for i in range(0, len(self.forest), 2):
				if i==len(self.forest)-1: #last tree if length of forest is odd-numbered
					if self.forest[i].key<currentMin.key:
						currentMin=self.forest[i]
						index=i
					pairedForest+=[self.forest[i]]#concatenate to new forest (no linking required)
				else:#pair trees
					if self.forest[i].key<=self.forest[i+1].key:
						if self.forest[i].leftChild==None:
							self.forest[i+1].parent=self.forest[i]
						else:
							self.forest[i+1].nextSibling=self.forest[i].leftChild
						self.forest[i].leftChild=self.forest[i+1]
						if self.forest[i].key<currentMin.key:
							currentMin=self.forest[i]
							index=i
						pairedForest+=[self.forest[i]]
						linkCount+=1
					else:
						if self.forest[i+1].leftChild==None:
							self.forest[i].parent=self.forest[i+1]
						else:
							self.forest[i].nextSibling=self.forest[i+1].leftChild
						self.forest[i+1].leftChild=self.forest[i]
						if self.forest[i+1].key<currentMin.key:
							currentMin=self.forest[i+1]
							index=i+1
						pairedForest+=[self.forest[i+1]]
						linkCount+=1
			self.forest=pairedForest
		#	print("Minimum key is {}.".format(currentMin.key))
			return (currentMin, linkCount)

	def delete_min(self):
		#finds and deletes min; restructures forest; returns number of linking operations
		linkCount=0
		(minNode, count) = self.find_min()
		linkCount+=count
		index=self.forest.index(minNode)#pretending forest is a linked list
		if minNode==None:
			print("Cannot delete min of empty heap")
			return (None, 0)
		#remove minNode from list
		elif index==0:
			self.forest=self.forest[1:]
		elif index==len(self.forest)-1:
			self.forest=self.forest[:index]
		else:			
			self.forest=self.forest[:index]+self.forest[index+1:]
		#print("Trying to delete min {} at index {}...".format(minNode.key, index))
		#concatenate children of minNode to list
		currentSibling=minNode.leftChild
		while currentSibling!=None:
			nextSibling=currentSibling.nextSibling
			self.forest+=[currentSibling]
			currentSibling.nextSibling=None
			currentSibling=nextSibling
		if len(self.forest)>0:
			self.forest[-1].parent=None #only for the last concatenated sibling as only this one carried parent pointer
		#print("Result of delete-min is {}.".format(self.listInorder()))
		return (minNode.key, linkCount)

	def decrease_key(self, node, diff):
		print("Trying to decrease key {} by {}...".format(node.key, diff))
		if node==None or diff<=0:
			return
		elif node.parent==None and node.nextSibling==None: #node is root
			node.key=node.key-diff
		else:
			self.unlink_node(node)
			node.key=node.key-diff
			self.forest+=[node]
		print("Result of decrease-key of key {} by {} is {}.".format(node.key, diff, self.listInorder()))

	def merge(self, heap2):
		#concatenates forests of this heap and heap2; returns number of link operations (always 0) for consistency
		#print("Trying to merge {} and {}.".format(self.listInorder(), heap2.listInorder()))
		self.forest+=heap2.forest
		#print("Result of merge is {}.".format(self.listInorder()))
		return 0

	def delete(self, node):
		if node==None:
			print("Cannot delete None")
			return
		elif node.parent==None and node.nextSibling==None: #node is root
			print("Trying to delete {}...".format(node.key))
			index=self.forest.index(node)#slight cheating; would be nicer to use a linked list as forest instead
			#remove node from forest list
			if index==0:
				self.forest=self.forest[1:]
			elif index==len(self.forest)-1:
				self.forest=self.forest[:-1]
			else:			
				self.forest=self.forest[:index]+self.forest[index+1:]
		else: #node is a child somewhere
			print("Trying to delete {}...".format(node.key))
			self.unlink_node(node)
		#concatenate potential children to forest list
		if node.leftChild!=None:
			sibling=node.leftChild
			while sibling!=None:
				self.forest+=[sibling]
				sibling=sibling.nextSibling
				if sibling!=None:
					self.forest[-1].nextSibling=None
				else:
					self.forest[-1].parent=None
		print("Result of deletion of {} is {}.".format(node.key, self.listInorder()))

				
	def unlink_node(self, node):
		#for non-root nodes only (does nothing about forest list, only tree-internal links)
		if node==None:
			return
		else:
			if node.nextSibling!=None:
				temp=node.nextSibling
				while temp.nextSibling!=None:#find rightmost child
					temp=temp.nextSibling
				if temp.parent.leftChild.key==node.key:#node is leftmost child (this comp requires unique keys!)
					#link parent to next sibling
					temp.parent.leftChild=node.nextSibling
					node.nextSibling=None
				else:
					#node is neither first nor last child of parent
					prevSibling=temp.parent.leftChild
					while prevSibling.nextSibling.key!=node.key:#find left (previous) sibling
						prevSibling=prevSibling.nextSibling
					prevSibling.nextSibling=node.nextSibling #cut out node, link left and right sibling
			else:
				#node is rightmost child of parent
				if node.parent.leftChild.key==node.key:
					#node is only child: just remove
					node.parent.leftChild=None
				else:
					prevSibling=node.parent.leftChild
					while prevSibling.nextSibling.key!=node.key:#find left (previous) sibling
						prevSibling=prevSibling.nextSibling
					prevSibling.parent=node.parent
					prevSibling.nextSibling=None
			node.parent=None
		#this was a somewhat messier implementation ostensibly doing the same, but it failed at least once in FTB heap
		#(I assume some pointer assignment is missing somewhere)
		"""elif node.nextSibling==None and node.parent.leftChild.key==node.key:#node is only child
			node.parent.leftChild=None
			node.parent=None	
		else:
			predecessor=None
			parent=None
			nextSibling=None
			if node.nextSibling==None:#node is last child
				parent=node.parent
				nextSibling=node.parent.leftChild
			else:
				nextSibling=node.nextSibling
			while nextSibling.key!=node.key:#find direct predecessor sibling
				predecessor=nextSibling
				if predecessor.nextSibling==None:
					parent=predecessor.parent
					nextSibling=predecessor.parent.leftChild
				else:
					nextSibling=predecessor.nextSibling
			if parent.leftChild.key==node.key:#node is leftmost child
				parent.leftChild=node.nextSibling
				node.nextSibling=None
			elif node.parent!=None:#node is rightmost child
				predecessor.parent=node.parent
				node.parent=None
			else:
				predecessor.nextSibling=node.nextSibling
				node.nextSibling=None"""
