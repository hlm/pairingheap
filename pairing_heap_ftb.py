#!/usr/bin/python3
from node import Node
from pairing_heap_interface import PairingHeapInterface

class PairingHeapFTB(PairingHeapInterface):
	#performs one front-to-back pairing+combining pass
	#uses two-pointer implementation from paper
	#TODO: left/right child (page 115)
	def __init__(self, root=None):
		self.root=root
		if self.root==None:
			print("created empty heap")

	def make_heap(self):
		#this is equivalent to init
		pass
	
	def listInorder(self, root):
		if(root==None):
			return []
		return self.listInorder(root.leftChild)+[root.key]+self.listInorder(root.nextSibling)

	def find_min(self):
		if self.root==None:
			return None
		else:
			return self.root

	def insert(self, node):
		#links node to root; returns number of linking operations
		linkCount=0
		#print("trying to insert {}...".format(node.key))
		if self.root==None: 
			#heap was empty before
			self.root=node
		else:
			newheap=PairingHeapFTB(node)
			linkCount+=self.merge(newheap)
		#print(self.listInorder(self.root))
		return linkCount

	def delete_min(self):
		#deletes root node and restructures heap; returns number of linking operations
		#print("trying to delete min...")
		minKey=None
		linkCount=0
		if self.root==None:
			print("Heap was already empty.")
			return (minKey, linkCount)
		elif self.root.leftChild==None:
			#heap contained only one element
			minKey=self.root.key
			self.root=None
		elif self.root.leftChild.nextSibling==None:
			#first child has no siblings->first child becomes root
			minKey=self.root.key
			self.root=self.root.leftChild
		else:
			minKey=self.root.key
			self.root=self.root.leftChild
			current=self.root
			nextSibling=None
			heaps=[]
			combined=None
			#left-to-right pairing+combining pass
			while current!=None:#create heaps of all orphaned children
				nextSibling=current.nextSibling
				heaps+=[PairingHeapFTB(current)]
				current.nextSibling=None
				current=nextSibling
			for j in range(0,len(heaps),2):
				if(j==len(heaps)-1):#last one
					if combined==None:
						combined=heaps[j]#if there is only one child
					else:
						linkCount+=combined.merge(heaps[j])#combine with the rest; count number of link ops
				else:
					heap=heaps[j]
					linkCount+=heap.merge(heaps[j+1])#pairing
					if combined==None:
						combined=heap#first pair
					else:
						linkCount+=combined.merge(heap)#combining
			self.root=combined.root
		#print("result is {}".format(self.listInorder(self.root)))	
		return (minKey, linkCount)

	def merge(self, heap2):
		#links this heap with heap2; returns the number of linking operations
		#print("Trying to merge {} and {}...".format(self.listInorder(self.root), self.listInorder(heap2.root)))
		linkCount=0
		if self.root==None:#heap is empty
			self.root=heap2.root
			return linkCount
		elif heap2.root==None:#heap 2 is empty
			return linkCount #this heap is the result
		else:
			if self.root.key<=heap2.root.key:
				heap2.root.nextSibling=self.root.leftChild
				if heap2.root.nextSibling==None:
					heap2.root.parent=self.root
				self.root.leftChild=heap2.root
				linkCount=1
			else:
				self.root.nextSibling=heap2.root.leftChild
				if self.root.nextSibling==None:
					self.root.parent=heap2.root
				heap2.root.leftChild=self.root
				self.root=heap2.root
				linkCount=1
			#TODO check for only children?
		#print("Result is {}".format(self.listInorder(self.root)))
		return linkCount

	def decrease_key(self, node, diff):#TODO more testing
		if self.root.key==node.key:
			self.root.key=self.root.key-diff
		else:
			#first step: cut node from heap
			self.unlink_node(node)#helper function
			#second step: decrease key
			subheap=PairingHeapFTB(node)
			subheap.root.key=subheap.root.key-diff
			#third step: merge back in
			self.merge(subheap)

	def delete(self, node): #TODO more testing?
		print("trying to delete {} from {}".format(node.key, self.listInorder(self.root)))
		if self.root.key==node.key:
			self.delete_min()
		else:
			self.unlink_node(node)#helper function
					
			subheap=PairingHeapFTB(node)
			subheap.delete_min()
			self.merge(subheap)
		print("result is {}".format(self.listInorder(self.root)))
		pass

	def unlink_node(self, node):
		#removes node from heap updating pointers
		if self.root.key==node.key:#remove the whole heap
			self.root=None
		else:
			if node.nextSibling!=None:
				temp=node.nextSibling
				while temp.nextSibling!=None:#find rightmost child
					temp=temp.nextSibling
				if temp.parent.leftChild.key==node.key:#node is leftmost child (this comp requires unique keys!)
					#link parent to next sibling
					temp.parent.leftChild=node.nextSibling
					node.nextSibling=None
				else:
					#node is neither first nor last child of parent
					prevSibling=temp.parent.leftChild
					while prevSibling.nextSibling.key!=node.key:#find left (previous) sibling
						prevSibling=prevSibling.nextSibling
					prevSibling.nextSibling=node.nextSibling #cut out node, link left and right sibling
			else:
				#node is rightmost child of parent
				if node.parent.leftChild.key==node.key:
					#node is only child: just remove
					node.parent.leftChild=None
				else:
					prevSibling=node.parent.leftChild
					while prevSibling.nextSibling.key!=node.key:#find left (previous) sibling
						prevSibling=prevSibling.nextSibling
					prevSibling.parent=node.parent
					prevSibling.nextSibling=None
			node.parent=None	
		
			
